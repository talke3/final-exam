import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { StudentDataComponent } from './student-data/student-data.component';
import { StudentdataFormComponent } from './studentdata-form/studentdata-form.component';
import { StudentmoreComponent } from './studentmore/studentmore.component';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent},
  { path: 'signup', component: SignUpComponent},
  { path: 'welcome', component: WelcomeComponent},  
  { path: 'studentsdata', component: StudentDataComponent},
  { path: 'studentsdataform', component: StudentdataFormComponent} ,
  { path: 'studentsmore', component: StudentmoreComponent} 

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
