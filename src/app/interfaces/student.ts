export interface Student {
    id:string,
    averageMath:number;
    psychoGrade:number;
    completeTuition:boolean;
    result?: string,
    status?: number

}
