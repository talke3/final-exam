import { Student } from './../interfaces/student';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ValidatorFn } from '@angular/forms';
import {FormControl} from '@angular/forms';
import { PredictService } from './../predict.service';
import { Router } from '@angular/router';

@Component({
  selector: 'studentdataform',
  templateUrl: './studentdata-form.component.html',
  styleUrls: ['./studentdata-form.component.css']
})
export class StudentdataFormComponent implements OnInit {
  
  students$; 
  students:Student[];
  @Input() averageMath:number;  
  @Input() psychoGrade:number;
  @Input() completeTuition:boolean;
  //@Input() result:string;
  //@Input() status:number;
  @Input() id:string; 
  @Input() formType:string;
  @Output() update = new EventEmitter<Student>();
  @Output() closeEdit = new EventEmitter<null>();
  @Output() isError:boolean = false;
  @Output() isErrorsat:boolean = false;
  disableSelect = new FormControl(false);
  

  updateParent(){
    let student:Student = {id:this.id, averageMath:this.averageMath, psychoGrade:this.psychoGrade,completeTuition:this.completeTuition};
    if(this.averageMath>100 || this.averageMath<0){
      this.isError = true;
    }else{
      this.update.emit(student); 
      if(this.formType == "Add student"){
        this.averageMath  = null;
        this.psychoGrade = null;
        this.completeTuition = null;
      }
      
    }
    if(this.psychoGrade>800 || this.psychoGrade<0){
      this.isErrorsat = true;
    }else{
      this.update.emit(student);
      if(this.formType == "Add student"){ 
        this.averageMath  = null;
        this.psychoGrade = null;
        this.completeTuition = null;
      }
    
  }
  }


  tellParentToClose(){
    this.closeEdit.emit(); 
  }

  public predict(student:Student){
    this.predictService.predict(this.averageMath,this.psychoGrade,this.completeTuition).subscribe(
      res => {
        console.log(res);
        if(res > 0.5){
          console.log('yes');
          student.result='will not drop';
          console.log(student.result);
        } else {
          console.log('no');
          student.result='will drop';
          console.log(student.result);
        }
        
      }
    )
  }

  constructor(private predictService:PredictService) { }

  ngOnInit(): void {
  }

 
}
