import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Student } from '../interfaces/student';
import { AngularFirestore } from '@angular/fire/firestore';
import { StudentService } from '../student.service';

@Component({
  selector: 'studentdata',
  templateUrl: './student-data.component.html',
  styleUrls: ['./student-data.component.css']
})
export class StudentDataComponent implements OnInit {
 
  students$; 
  students:Student[]; 
  editstate = [];
  addStudentFormOpen = false;
  panelOpenState = false;


  constructor(private studentService:StudentService, public authService:AuthService,private db:AngularFirestore) { }

  deleteStudent(id:string){
    this.studentService.deleteStudent(id);
  }

  update(student:Student){
    this.studentService.updateStudent(student.id,student.averageMath,student.psychoGrade,student.completeTuition,student.result,student.status);
  }

  add(student:Student){
    this.studentService.addStudent(student.averageMath,student.psychoGrade,student.completeTuition,student.result,student.status)
  }
  
  ngOnInit(): void {
    this.students$ = this.studentService.getStudent(); 
    this.students$.subscribe(
      docs =>{
        console.log('worked');           
        this.students = [];
        for(let document of docs){
          const student:Student = document.payload.doc.data();
          student.id = document.payload.doc.id; 
          this.students.push(student); 
        }
      }
    ) 
}

}





