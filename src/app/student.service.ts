import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StudentService {
  studentCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 

  public getStudent(){
    this.studentCollection = this.db.collection(`/students`); 
    return this.studentCollection.snapshotChanges()      
  } 

  
  deleteStudent(id:string){
    this.db.doc(`/students/${id}`).delete(); 
  } 

  
  addStudent(averageMath:number,psychoGrade:number,completeTuition:boolean,result:string,status:number){
    const student = {averageMath:averageMath,psychoGrade:psychoGrade,completeTuition:completeTuition,result:result,status:status}; 
    this.studentCollection.add(student);
  }


  updateStudent(id:string,averageMath:number,psychoGrade:number,completeTuition:boolean,result:string,status:number){
    this.db.doc(`/students/${id}`).update(
      {
        averageMath:averageMath,
        psychoGrade:psychoGrade,
        completeTuition:completeTuition,
        result:result,
        status:status

      }
    )
  }
  


  constructor(private db:AngularFirestore) { }
}